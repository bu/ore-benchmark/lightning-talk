\newlength{\hardcodedheight}
\newlength{\askip}
\newlength{\bskip}
\newlength{\blskip}

\setlength{\hardcodedheight}{90pt}
\setlength{\askip}{-20pt}
\setlength{\bskip}{-2pt}
\setlength{\blskip}{0pt}

\section{Motivation}

	\begin{frame}{Secure Range Queries}
		
		\begin{block}{The problem}
			
			\begin{itemize}
				\item Many different solutions
				\item Understanding performance / security tradeoff
				\item Heterogeneous security definitions and leakage profiles
				\item \textbf{Performance not well-understood}
				\begin{itemize}
					\item Some constructions were not even implemented
					\item Most constructions have at most prototype implementation
					\item Most of them were not benchmarked against one another
					\item Constructions use different primitive implementations
				\end{itemize}
			\end{itemize}

		\end{block}

		\begin{fixnote}
			Explain the task --- secure range queries.
			Explain the security model --- snapshot model.
			Note that there are many solutions, with different performance and security guarantees.
			Stress the heterogeneity and lack of implementations.
		\end{fixnote}
	\end{frame}

	\begin{frame}{Secure Range Queries}
		
		\begin{block}{The solution}
			
			\begin{itemize}
				\item Analysed security and leakages of the constructions under a common framework
				\item Analysed theoretically performance of the constructions
				\item \textbf{Implemented and run experiments}
				\begin{itemize}
					\item Implemented 5 OPE / ORE schemes and 5 range query protocols
					\item Used same language, framework and primitive implementations
					\item Benchmarked primitives execution times
					\item Simulated insertion and query stages of all protocols
					\begin{itemize}
						\item OPE / ORE schemes use our implementation of {\BPlus} tree
					\end{itemize}
					\item Used different data sizes and distributions, query loads, cache policies and parameters of the constructions
				\end{itemize}
			\end{itemize}

		\end{block}

		\begin{fixnote}
			Say that we analyzed security definitions, leakages and performances theoretically.
			Mention what we mean by performance --- running time that depends heavily on {\IO}s, communication volume and size.
			Note that constructions rely on crypto primitives, that we benchmarked them, used the same implementations and analyzed usage complexity.
			Say that we have run extensive experiments on different data and queries.
		\end{fixnote}
		
	\end{frame}

\section{Results}
	
	\begin{frame}{OPE / ORE schemes}
		
		\begin{lrbox}{\tableprimitivebox}%
			
			\begin{tabular}{ l c c c c c }
		
				\toprule
		
				\multirow{2}{*}{Scheme}						& \multicolumn{2}{c}{\onslide<2->{Primitive usage}}																						& \onslide<2->{Ciphertext size,}																& \onslide<2->{Leakage}																\onslide<2->{\\ \cline{2-3}}
				\rule{0pt}{10pt}							& \onslide<2->{Encryption}													& \onslide<2->{Comparison}									& \onslide<2->{or state size}																	& \onslide<2->{(In addition to inherent total order)}								\\
				
				\toprule
											
				BCLO~\cite{crypt-db-ope}					& \onslide<3->{$\bm{n}$ \textbf{HG}}										& \onslide<3->{none}										& \onslide<3->{$2n$}																			& \onslide<3->{\textbf{$\approx$ Top half of the bits}}								\\
											
				\midrule
				
				CLWW~\cite{practical-ore}					& \onslide<3->{$n$ PRF} 													& \onslide<3->{none}										& \onslide<3->{$2n$}																			& \onslide<3->{\textbf{Most-significant differing bit}}								\\
		
				\midrule
		
				\multirow{3}{*}{Lewi-Wu~\cite{lewi-ore}}	& \onslide<3->{\boldmath{} $\nicefrac{2n}{d}$ \unboldmath{} \textbf{PRP}}	& \onslide<3->{\multirow{3}{*}{$\frac{n}{2d}$ Hash}}		& \onslide<3->{\multirow{3}{*}{$\frac{n}{d} \left(\lambda + n + 2^{d + 1} \right) + \lambda$}}	& \onslide<3->{\multirow{3}{*}{Most-significant differing block}}					\\
															& \onslide<3->{$2 \frac{n}{d} \left( 2^d + 1 \right)$ PRF}					&															&																								&																					\\
															& \onslide<3->{$\frac{n}{d} 2^d$ Hash}										&															&																								&																					\\
		
				\midrule
				
				\multirow{3}{*}{CLOZ~\cite{adam-ore-v2}}	& \onslide<3->{$n$ PRF}														& \onslide<3->{\multirow{3}{*}{$\bm{n^2}$ \textbf{PPH}}}	& \onslide<3->{\multirow{3}{*}{$n \cdot h$}}													& \onslide<3->{\multirow{3}{*}{Equality pattern of most-significant differing bit}}	\\
															& \onslide<3->{$n$ PPH}														&															&																								&																					\\
															& \onslide<3->{1 PRP}														&															&																								&																					\\
				
				\midrule
		
				FH-OPE~\cite{fh-ope}						& \onslide<3->{1 Traversal}													& \onslide<3->{3 Traversals}								& \onslide<3->{$\bm{3 \cdot n \cdot N}$}														& \onslide<3->{Insertion order}														\\
		
				\bottomrule
		
			\end{tabular}

		\end{lrbox}%
		
		\begin{center}

			\scalebox{0.5}{\usebox{\tableprimitivebox}}

		\end{center}

		\begin{fixnote}
			Iterate over columns --- the values we measure.
			Say that we picked the weakest points of every scheme.
			Mention that we ranked them by security.
			Stress the importance of primitive usage as opposed to time benchmarks.
		\end{fixnote}
		
	\end{frame}
	
	\begin{frame}{Range query protocols}
		
		\begin{lrbox}{\tableprotocolbox}%
			
			\begin{tabular}{ l c c c c c c }

				\toprule
		
				\multirow{2}{*}{Protocol}				& \multicolumn{2}{c}{\onslide<2->{{\IO} requests}}																																& \multirow{2}{*}{\onslide<2->{Leakage}}	& \multicolumn{2}{c}{\onslide<2->{Communication (result excluded)}}																&	\onslide<2->{\\ \cline{2-3} \cline{5-6}}
				\rule{0pt}{10pt}						& \onslide<2->{Construction}									& \onslide<2->{Query}																							&											& \onslide<2->{Construction}									& \onslide<2->{Query} 											&	\\
		
				\toprule
		
				{\BPlus} tree with ORE					& \onslide<3->{$\log_B \frac{N}{B}$}							& \onslide<3->{$\log_B \frac{N}{B} + \frac{r}{B}$}																& \onslide<3->{\textbf{Same as ORE}}		& \onslide<3->{$1$}												& \onslide<3->{$1$}												&	\\
				\midrule
		
				Kerschbaum~\cite{florian-protocol}		& \onslide<3->{$\bm{\frac{N}{B}}$}								& \onslide<3->{$\log_2 \frac{N}{B} + \frac{r}{B}$}																& \onslide<3->{\textbf{Total order}}		& \onslide<3->{$\log_2 N$}										& \onslide<3->{$\log_2 N$}										&	\\
		
				\midrule
		
				POPE~\cite{pope} warm					& \multirow{2}{*}{\onslide<3->{$1$}}							& \onslide<3->{$\log_L \frac{N}{B} + \frac{r}{B}$}																& \onslide<3->{\textbf{Partial order}}		& \multirow{2}{*}{\onslide<3->{$1$}}							& \onslide<3->{$\log_L N$}										&	\\
				
				POPE~\cite{pope} cold					& 																& \onslide<3->{$\bm{{\nicefrac{N}{B}}}$}																		& \onslide<3->{Fully hiding}				& 																& \onslide<3->{$\bm{N}$}										&	\\
				
				\midrule
		
				Logarithmic-BRC~\cite{ioannis-protocol}	& \onslide<3->{\textbf{---}}									& \onslide<3->{$\bm{r}$}																						& \onslide<3->{Same as SSE}					& \onslide<3->{\textbf{---}}									& \onslide<3->{$\log_2 N$}										&	\\
		
				\midrule
		
				\multirow{2}{*}{ORAM}					& \multirow{2}{*}{\onslide<3->{$\bm{{ \log^2 \frac{N}{B} }}$}}	& \multirow{2}{*}{\onslide<3->{$\bm{{ \log_2 \frac{N}{B} \left( \log_B \frac{N}{B} + \frac{r}{B} \right) }}$}}	& \onslide<3->{Fully hiding}				& \multirow{2}{*}{\onslide<3->{$\bm{{ \log^2 \frac{N}{B} }}$}}	& \multirow{2}{*}{\onslide<3->{$\bm{{ \log^2 \frac{N}{B} }}$}}	&	\\
														&																&																												& \onslide<3->{(access pattern)}			&																&																&	\\
			
				\bottomrule
		
			\end{tabular}		
			
		\end{lrbox}%
		
		\begin{center}

			\scalebox{0.5}{\usebox{\tableprotocolbox}}

		\end{center}
		
		\begin{fixnote}
			Iterate over columns --- the values we measure.
			Say that we picked the weakest points of every protocol.
			Mention that we ranked them by security.
			Stress the importance of {\IO} complexity and communication values as opposed to time benchmarks.
		\end{fixnote}
		
	\end{frame}
	
	\begin{frame}{Simulation results}

		\begin{minipage}{\linewidth}

			\begin{figure}[H]
				\captionsetup[subfigure]{justification=centering}
				\centering
				\begin{subfigure}[t]{0.333\textwidth}
					\centering
					\includegraphics[height=\hardcodedheight,width=\linewidth]{protocol-charts-cios.pdf}
					\setlength{\abovecaptionskip}{\askip}
					\setlength{\belowcaptionskip}{\bskip}
					\caption{\tiny Construction stage number of {\IO} requests}
				\end{subfigure}%
				\onslide<2->{~ % chktex 39
					\begin{subfigure}[t]{0.333\textwidth}
						\centering
						\includegraphics[height=\hardcodedheight,width=\linewidth]{protocol-charts-cvol.pdf}
						\setlength{\abovecaptionskip}{\askip}
						\setlength{\belowcaptionskip}{\bskip}
						\caption{\tiny Construction stage communication volume (number of messages)}
					\end{subfigure}%
				}\onslide<3->{~ % chktex 39
					\begin{subfigure}[t]{0.333\textwidth}
						\centering
						\includegraphics[height=\hardcodedheight,width=\linewidth]{protocol-charts-csize.pdf}
						\setlength{\abovecaptionskip}{\askip}
						\setlength{\belowcaptionskip}{\bskip}
						\caption{\tiny Construction stage communication size (bytes transferred)}
					\end{subfigure}%
				}

				\begin{subfigure}[t]{0.333\textwidth}
					\centering
					\includegraphics[height=\hardcodedheight,width=\linewidth]{protocol-charts-qios.pdf}
					\setlength{\abovecaptionskip}{\askip}
					\setlength{\belowcaptionskip}{\blskip}
					\caption{\tiny Queries stage number of {\IO} requests}
				\end{subfigure}%
				\onslide<2->{~ % chktex 39
					\begin{subfigure}[t]{0.333\textwidth}
						\centering
						\includegraphics[height=\hardcodedheight,width=\linewidth]{protocol-charts-qvol.pdf}
						\setlength{\abovecaptionskip}{\askip}
						\setlength{\belowcaptionskip}{\blskip}
						\caption{\tiny Queries stage communication volume (number of messages)}
					\end{subfigure}%
				}\onslide<3->{~ % chktex 39
					\begin{subfigure}[t]{0.333\textwidth}
						\centering
						\includegraphics[height=\hardcodedheight,width=\linewidth]{protocol-charts-qsize.pdf}
						\setlength{\abovecaptionskip}{\askip}
						\setlength{\belowcaptionskip}{\blskip}
						\caption{\tiny Queries stage communication size (bytes transferred, log scale)}
					\end{subfigure}%
				}
			\end{figure}
			
		\end{minipage}

		\begin{fixnote}
			Iterate over plots explaining the setup and what was measured.
			Do not go into details, the purpose is to show \emph{what} and \emph{how} we measured, not the results.
		\end{fixnote}		
		
	\end{frame}

	\begin{frame}{Conclusions}
			
		\begin{itemize}
			\item Strengths, weaknesses and use cases for each construction
			\item Ranked by security and performance
			\item Bugs in algorithms
			\item Security vulnerabilities in implementations
			\item Positive performance results for ORAM with {\BPlus} tree, and Logarithmic-BRC
		\end{itemize}

		\begin{fixnote}
			Say that we have found improvements, bugs or vulnerabilities for most of the constructions.
			Mention that we experimentally proved the use cases for each.
			Note that some of the constructions showed surprisingly positive results.
		\end{fixnote}
		
	\end{frame}
