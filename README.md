# Lightning Talk at Brown on VLDB 2019 work

The up-to-date version of the document is built in CI and resides as artifact.

- [Download the file](https://git.dbogatov.org/bu/ore-benchmark/lightning-talk/-/jobs/artifacts/master/raw/presentation.pdf?job=artifacts)
- [View the file](https://git.dbogatov.org/bu/ore-benchmark/lightning-talk/-/jobs/artifacts/master/file/presentation.pdf?job=artifacts)

## How to compile

```bash
./document/build.sh # to compile without notes
./document/build.sh -n # to compile wit notes
open ./document/dist/*.pdf # to open
```
